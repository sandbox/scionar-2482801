<?php
/**
 * @file
 *   Includes admin page callbacks and functions.
 */

/**
 * Form constructor for general configurations page.
 */
function paytrail_general_form($form, &$form_state) {
  $form['merchant'] = array(
    '#type' => 'fieldset',
    '#title' => t('Merchant general'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );

  $form['merchant']['paytrail_merchant_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Merchant ID'),
    '#default_value' => variable_get('paytrail_merchant_id', ''),
    '#size' => 11,
    '#maxlength' => 11,
    '#description' => t('Merchant\'s ID in Paytrail service'),
    '#required' => TRUE,
  );

  $form['merchant']['paytrail_merchant_secret'] = array(
    '#type' => 'textfield',
    '#title' => t('Merchant\'s secret hash'),
    '#default_value' => variable_get('paytrail_merchant_secret', ''),
    '#size' => 30,
    '#maxlength' => 40,
    '#description' => t('Merchant\'s secret hash in Paytrail service'),
    '#required' => TRUE,
  );

  $form['method'] = array(
    '#type' => 'fieldset',
    '#title' => t('Used method'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );

  $methods = array(
    'connectapi' => t('Connect API'),
    'restinterface' => t('REST Interface'),
    'forminterface' => t('Form Interface'),
  );
  $form['method']['paytrail_methods'] = array(
    '#type' => 'radios',
    '#default_value' => variable_get('paytrail_methods', 'connectapi'),
    '#options' => $methods,
  );

  return system_settings_form($form);
}

/**
 * Form constructor for Connect API configuration page.
 */
function paytrail_connect_api_form($form, &$form_state) {
  $form['access'] = array(
    '#type' => 'fieldset',
    '#title' => t('Access request'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );

  $form['access']['paytrail_connect_api_delivery_address_access'] = array(
    '#type' => 'checkbox',
    '#title' => t('Demand delivery address access'),
    '#default_value' => variable_get('paytrail_connect_api_delivery_address_access', 0),
  );

  $form['locale'] = array(
    '#type' => 'fieldset',
    '#title' => t('Locale'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );

  $locales = array(
    'fi_FI' => t('Finnish FI'),
    'en_US' => t('English US'),
    'se_SV' => t('Swedish SE'),
  );
  $form['locale']['paytrail_connect_api_locale'] = array(
    '#type' => 'radios',
    '#default_value' => variable_get('paytrail_connect_api_locale', ''),
    '#options' => $locales,
  );

  $form['data'] = array(
    '#type' => 'fieldset',
    '#title' => t('Payment data'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );

  $form['data']['paytrail_connect_api_order_number'] = array(
    '#type' => 'textfield',
    '#title' => t('Order number'),
    '#default_value' => variable_get('paytrail_connect_api_order_number', ''),
    '#size' => 11,
    '#description' => t('Can be used to identify payments in Merchant\'s Panel'),
  );

  return system_settings_form($form);
}

/**
 * Form constructor for REST and form interface configuration page.
 */
function paytrail_restinterface_form($form, &$form_state) {
  $form['payment_data'] = array(
    '#type' => 'fieldset',
    '#title' => t('Payment data'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['payment_data']['paytrail_interface_order_number'] = array(
    '#title' => t('Order number'),
    '#default_value' => variable_get('paytrail_interface_order_number', ''),
    '#type' => 'textfield',
    '#size' => 60,
  );

  $form['payment_data']['paytrail_interface_reference_number'] = array(
    '#title' => t('Reference number'),
    '#default_value' => variable_get('paytrail_interface_reference_number', ''),
    '#type' => 'textfield',
    '#size' => 60,
  );

  $form['payment_data']['paytrail_interface_describtion'] = array(
    '#title' => t('Order describtion'),
    '#default_value' => variable_get('paytrail_interface_describtion', ''),
    '#type' => 'textarea',
  );

  $form['payment_data']['paytrail_interface_culture'] = array(
    '#title' => t('Culture'),
    '#default_value' => variable_get('paytrail_interface_culture', ''),
    '#type' => 'radios',
    '#options' => array(
      'fi_FI' => t('fi_FI'),
      'sv_SE' => t('sv_SE'),
      'en_US' => t('en_US'),
    ),
  );

  $form['payment_data']['paytrail_interface_embedding'] = array(
    '#type' => 'checkbox',
    '#default_value' => variable_get('paytrail_interface_embedding', 0),
    '#title' => t('Payment selection embedding'),
  );

  $form['version'] = array(
    '#type' => 'fieldset',
    '#title' => t('Payment version'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['version']['paytrail_interface_version'] = array(
    '#type' => 'radios',
    '#default_value' => variable_get('paytrail_interface_version', 'S1'),
    '#options' => array(
      'S1' => t('S1'),
      'E1' => t('E1'),
    ),
  );

  $form['s1_specific'] = array(
    '#type' => 'fieldset',
    '#title' => t('S1 Payment specific data'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['s1_specific']['paytrail_interface_amount'] = array(
    '#title' => t('Amount'),
    '#default_value' => variable_get('paytrail_interface_amount', ''),
    '#type' => 'textfield',
    '#size' => 60,
  );

  $form['e1_specific'] = array(
    '#type' => 'fieldset',
    '#title' => t('E1 Payment specific data'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['e1_specific']['paytrail_interface_vat'] = array(
    '#title' => t('Include VAT'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('paytrail_interface_vat', ''),
  );

  return system_settings_form($form);
}

/**
 * Form validation handler for paytrail_restinterface_form().
 */
function paytrail_restinterface_form_validate($form, &$form_state) {
  $form_state['values']['paytrail_interface_amount'] = _paytrail_string_to_float($form_state['values']['paytrail_interface_amount']);
}

/**
 * Creates theme table of products from paytrail_product table in database.
 *
 * @return string
 *   HTML for product table with all products.
 */
function _paytrail_product_table() {
  $header = array(
    'code',
    'name',
    'quantity',
    'price',
    'payment type',
    '',
  );

  $sql = 'SELECT pid, code, name, quantity, price, vat, discount, payment_type FROM {paytrail_product}';
  $result = db_query($sql);
  $t_rows = array();

  if ($result) {
    while ($p_row = $result->fetchAssoc()) {
      $t_rows[] = array(
        $p_row['code'],
        $p_row['name'],
        $p_row['quantity'],
        $p_row['price'],
        $p_row['payment_type'],
        l('Edit', 'admin/config/content/paytrail/product/edit/' . $p_row['pid']) . ' ' . l('Remove', 'admin/config/content/paytrail/product/remove/' . $p_row['pid']),
      );
    }
  }

  $variables = array(
    'header' => $header,
    'rows' => $t_rows,
    'attributes' => array(),
    'caption' => NULL,
    'colgroups' => array(),
    'sticky' => TRUE,
    'empty' => t('No products set.'),
  );

  $theme = theme_table($variables);

  return $theme;
}

/**
 * Form constructor for product display page.
 */
function paytrail_product_manage_form($form, &$form_state) {
  $form['paytrail_introduction_text'] = array(
    '#markup' => '<p>This form let\'s administrators add and edit order products.</p>',
  );

  $form['paytrail_product_table'] = array(
    '#markup' => _paytrail_product_table(),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Create new product',
  );

  return $form;
}

/**
 * Form validation handler for paytrail_product_manage_form().
 */
function paytrail_product_manage_form_validate($form, &$form_state) {
  $form_state['values']['code'] = (is_string($form_state['values']['code'])) ? strval($form_state['values']['code']) : NULL;
  $form_state['values']['name'] = (is_string($form_state['values']['name'])) ? strval($form_state['values']['name']) : NULL;
  $form_state['values']['quantity'] = _paytrail_string_to_float($form_state['values']['quantity']);
  $form_state['values']['price'] = _paytrail_string_to_float($form_state['values']['price']);
  $form_state['values']['vat'] = _paytrail_string_to_float($form_state['values']['vat']);
  $form_state['values']['discount'] = _paytrail_string_to_float($form_state['values']['discount']);
  $form_state['values']['product_type'] = (is_string($form_state['values']['product_type'])) ? intval($form_state['values']['product_type']) : NULL;
  $form_state['values']['payment_type '] = (is_string($form_state['values']['payment_type'])) ? strval($form_state['values']['payment_type']) : NULL;
}

/**
 * Form constructor for product creating page.
 */
function paytrail_product_manage_add_form($form, &$form_state) {
  $form['payment_type'] = array(
    '#type' => 'select',
    '#title' => t('Payment type'),
    '#options' => array(
      'A1' => t('A1'),
      'E1' => t('E1'),
    ),
    '#default_value' => 0,
    '#description' => t('A1 is used by Connect API and E1 by REST and Form Interface.'),
  );

  $form['code'] = array(
    '#type' => 'textfield',
    '#title' => t('Code'),
    '#size' => 18,
    '#maxlength' => 16,
    '#required' => FALSE,
  );

  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#size' => 20,
    '#maxlength' => 64,
    '#required' => FALSE,
  );

  $form['quantity'] = array(
    '#type' => 'textfield',
    '#title' => t('Quantity'),
    '#size' => 9,
    '#required' => TRUE,
  );

  $form['price'] = array(
    '#type' => 'textfield',
    '#title' => t('Unit price'),
    '#size' => 9,
    '#required' => TRUE,
  );

  $form['vat'] = array(
    '#type' => 'textfield',
    '#title' => t('Vat percentage / Tax'),
    '#size' => 9,
    '#required' => TRUE,
  );

  $form['discount'] = array(
    '#type' => 'textfield',
    '#title' => t('Discount of row'),
    '#size' => 9,
    '#maxlength' => 10,
    '#description' => t('Used only on E1 payment.'),
    '#required' => FALSE,
  );

  $form['product_type'] = array(
    '#type' => 'radios',
    '#title' => t('Type of product'),
    '#default_value' => variable_get('paytrail_interface_version', 'S1'),
    '#options' => array(
      1 => t('Normal product'),
      2 => t('Shipment costs'),
      3 => t('Processing costs'),
    ),
    '#description' => t('Used only on E1 payment.'),
    '#required' => FALSE,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Create',
  );

  return $form;
}

/**
 * Form submission handler for paytrail_product_manage_add_form().
 */
function paytrail_product_manage_add_form_submit($form, &$form_state) {
  db_insert('paytrail_product')
    ->fields(array(
      'code' => $form_state['values']['code'],
      'name' => $form_state['values']['name'],
      'quantity' => $form_state['values']['quantity'],
      'price' => $form_state['values']['price'],
      'vat' => $form_state['values']['vat'],
      'discount' => $form_state['values']['discount'],
      'product_type' => $form_state['values']['product_type'],
      'payment_type' =>$form_state['values']['payment_type'],
    ))->execute();

    $form_state['redirect'] = 'admin/config/content/paytrail/product';

    drupal_set_message(t('Product inserted with success.'));
}

/**
 * Form constructor for product editing page.
 *
 * @param int $pid
 *   Id of edited product.
 */
function paytrail_product_manage_edit_form($form, &$form_state, $pid) {
  $sql = 'SELECT pid, code, name, quantity, price, vat, discount, product_type, payment_type FROM {paytrail_product} WHERE pid = :pid';
  $result = db_query($sql, array(':pid' => $pid));

  $row = $result->fetchAssoc();

  $form['pid'] = array(
    '#type' => 'hidden',
    '#value' => $row['pid'],
  );

  $form['payment_type'] = array(
    '#type' => 'select',
    '#title' => t('Payment type'),
    '#options' => array(
      'A1' => t('A1'),
      'E1' => t('E1'),
    ),
    '#default_value' => $row['payment_type'],
    '#description' => t('A1 is used by Connect API and E1 by REST and Form Interface.'),
  );

  $form['code'] = array(
    '#type' => 'textfield',
    '#title' => t('Code'),
    '#size' => 18,
    '#maxlength' => 16,
    '#default_value' => $row['code'],
    '#required' => FALSE,
  );

  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#size' => 20,
    '#maxlength' => 64,
    '#default_value' => $row['name'],
    '#required' => FALSE,
  );

  $form['quantity'] = array(
    '#type' => 'textfield',
    '#title' => t('Quantity'),
    '#size' => 9,
    '#default_value' => $row['quantity'],
    '#required' => TRUE,
  );

  $form['price'] = array(
    '#type' => 'textfield',
    '#title' => t('Unit price'),
    '#size' => 9,
    '#default_value' => $row['price'],
    '#required' => TRUE,
  );

  $form['vat'] = array(
    '#type' => 'textfield',
    '#title' => t('Vat percentage / Tax'),
    '#size' => 9,
    '#default_value' => $row['vat'],
    '#required' => TRUE,
  );

  $form['discount'] = array(
    '#type' => 'textfield',
    '#title' => t('Discount of row'),
    '#size' => 9,
    '#maxlength' => 10,
    '#default_value' => $row['discount'],
    '#description' => t('Used only on E1 payment.'),
    '#required' => FALSE,
  );

  $form['product_type'] = array(
    '#type' => 'radios',
    '#title' => t('Type of product'),
    '#default_value' => $row['product_type'],
    '#options' => array(
      1 => t('Normal product'),
      2 => t('Shipment costs'),
      3 => t('Processing costs'),
    ),
    '#description' => t('Used only on E1 payment.'),
    '#required' => FALSE,
  );

  $form['cancel'] = array(
    '#type' => 'button',
    '#value' => t('Cancel'),
    '#submit' => array('paytrail_product_manage_edit_form_cancel'),
    '#executes_submit_callback' => FALSE,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Save product',
  );

  return $form;
}

/**
 * Callback for cancel button in paytrail_product_manage_edit_form();
 */
function paytrail_product_manage_edit_form_cancel() {
  drupal_goto('admin/config/content/paytrail/product');
}

/**
 * Form submission handler for paytrail_product_manage_edit_form().
 */
function paytrail_product_manage_edit_form_submit($form, &$form_state) {
  $num_updatet = db_update('paytrail_product')
    ->fields(array(
      'code' => $form_state['values']['code'],
      'name' => $form_state['values']['name'],
      'quantity' => $form_state['values']['quantity'],
      'price' => $form_state['values']['price'],
      'vat' => $form_state['values']['vat'],
      'discount' => $form_state['values']['discount'],
      'product_type' => $form_state['values']['product_type'],
      'payment_type' =>$form_state['values']['payment_type'],
    ))
    ->condition('pid', $form_state['values']['pid'])
    ->execute();

  $form_state['redirect'] = 'admin/config/content/paytrail/product';

  drupal_set_message(t('Product updatet with success.'));
}

/**
 * Form constructor for product removing page.
 *
 * @param int $pid
 *   Id of edited product.
 */
function paytrail_product_manage_remove_form($form, &$form_state, $pid) {
  $sql = 'SELECT code, name, quantity, price, vat, discount, product_type, payment_type FROM {paytrail_product} WHERE pid = :pid';
  $result = db_query($sql, array(':pid' => $pid));

  $row = $result->fetchAssoc();

  $form['pid'] = array(
    '#type' => 'hidden',
    '#value' => t($pid),
  );

  $form['question'] = array(
    '#type' => 'item',
    '#title' => t('Are you sure you want delete this product?'),
  );

  $form['code'] = array(
    '#type' => 'item',
    '#markup' => 'Code: ' . $row['code'],
  );

  $form['name'] = array(
    '#type' => 'item',
    '#markup' => 'Name: ' . $row['name'],
  );

  $form['quantity'] = array(
    '#type' => 'item',
    '#markup' => 'Quantity: ' . $row['quantity'],
  );

  $form['price'] = array(
    '#type' => 'item',
    '#markup' => 'Price: ' . $row['price'],
  );

  $form['vat'] = array(
    '#type' => 'item',
    '#markup' => 'Vat: ' . $row['vat'],
  );

  $form['discount'] = array(
    '#type' => 'item',
    '#markup' => 'Discount: ' . $row['discount'],
  );

  $form['product_type'] = array(
    '#type' => 'item',
    '#markup' => 'Product type: ' . $row['product_type'],
  );

  $form['payment_type'] = array(
    '#type' => 'item',
    '#markup' => 'Payment type: ' . $row['payment_type'],
  );

  $form['cancel'] = array(
    '#type' => 'button',
    '#value' => t('Cancel'),
    '#submit' => array('paytrail_product_manage_remove_form_cancel'),
    '#executes_submit_callback' => FALSE,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Remove product',
  );

  return $form;
}

/**
 * Callback for cancel button in paytrail_product_manage_remove_form().
 */
function paytrail_product_manage_remove_form_cancel($form, &$form_state) {
  drupal_goto('admin/config/content/paytrail/product');
}

/**
 * Form submission handler for paytrail_restinterface_prepare_form().
 */
function paytrail_product_manage_remove_form_submit($form, &$form_state) {
  $pid = $form_state['values']['pid'];
  $num_deleted = db_delete('paytrail_product')
    ->condition('pid', $pid)
    ->execute();

  $form_state['redirect'] = 'admin/config/content/paytrail/product';

  drupal_set_message(t('Product removed with success.'));
}