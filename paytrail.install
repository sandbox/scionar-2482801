<?php

    function paytrail_schema() {
        $schema = array();
        
        $schema['paytrail_product'] = array(
            'description' => 'Paytrail product table.',
            'fields' => array(
                'pid' => array(
                    'description' => 'The primary identifier for a product.',
                    'type' => 'serial',
                    'unsigned' => TRUE,
                    'not null' => TRUE,
                ),
                'code' => array(
                    'description' => 'Product code for actual product.',
                    'type' => 'varchar',
                    'length' => 16,
                    'default' => NULL,
                ),
                'name' => array(
                    'description' => 'Name of product.',
                    'type' => 'varchar',
                    'length' => 255,
                    'default' => NULL,
                ),
                'quantity' => array(
                    'description' => 'Quantity of products.',
                    'type' => 'float',
                    'default' => NULL,
                    'unsigned' => TRUE,
                ),
                'price' => array(
                    'description' => 'Price of one product.',
                    'type' => 'float',
                    'default' => NULL,
                    'unsigned' => TRUE,
                ),
                'vat' => array(
                    'description' => 'Value-added tax.',
                    'type' => 'float',
                    'default' => NULL,
                    'unsigned' => TRUE,
                ),
                'discount' => array(
                    'description' => 'Product discount. Only in E1 specification.',
                    'type' => 'float',
                    'default' => NULL,
                    'unsigned' => TRUE,
                ),
                'product_type' => array(
                    'description' => 'Cost type. Only in E1 specification.',
                    'type' => 'int',
                    'size' => 'tiny',
                    'default' => NULL,
                ),
                'payment_type' => array(
                    'description' => 'Payment type related to product.',
                    'type' => 'varchar',
                    'length' => 2,
                    'default' => NULL,
                ),
            ),
            'primary key' => array(
                'pid',
            ),
        );

        return $schema;
    }